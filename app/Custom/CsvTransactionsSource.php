<?php

namespace App\Custom;

use Illuminate\Support\Facades\Storage;

/**
 * Allows retrieving transactions from a CSV file.
 */
class CsvTransactionsSource implements TransactionsSourceInterface
{

    /**
     * Location of the transactions file relative to storage/app
     */
    const TRANSACTIONS_FILE = 'transactions.csv';

    /**
     * {@inheritDoc}
     */
    function retrieve()
    {
        if (!Storage::disk('local')->exists(CsvTransactionsSource::TRANSACTIONS_FILE)) {
            throw new \Exception('File not found at ' . CsvTransactionsSource::TRANSACTIONS_FILE);
        }

        // Trim required to remove last empty line, could be array_filter too
        $contents = explode(PHP_EOL, trim(Storage::disk('local')->get(CsvTransactionsSource::TRANSACTIONS_FILE)));

        $column_names = str_getcsv(array_shift($contents));

        return array_map(function ($row) use ($column_names) {
            $row = str_getcsv($row);
            return (object)array_combine($column_names, $row);
        }, $contents);
    }

}
