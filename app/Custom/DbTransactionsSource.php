<?php

namespace App\Custom;

use Illuminate\Support\Facades\DB;

/**
 * Allows retrieving transactions from database.
 */
class DbTransactionsSource implements TransactionsSourceInterface
{

    /**
     * {@inheritDoc}
     */
    function retrieve()
    {
        return DB::table('transactions')->get()->all();
    }

}
