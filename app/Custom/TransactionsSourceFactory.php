<?php

namespace App\Custom;

class TransactionsSourceFactory
{

    /**
     * @var TransactionsSourceInterface[]
     */
    protected static $instances;

    /**
     * @param  string  $source
     *
     * @return TransactionsSourceInterface
     *
     * @throws \Exception
     */
    static function getInstance(string $source): TransactionsSourceInterface
    {
        if (!isset(TransactionsSourceFactory::$instances[$source])) {
            switch ($source) {
                case 'db':
                    TransactionsSourceFactory::$instances[$source] = new DbTransactionsSource();
                    break;
                case 'csv':
                    TransactionsSourceFactory::$instances[$source] = new CsvTransactionsSource();
                    break;
                default:
                    throw new \Exception('Unknown transaction source');
            }
        }

        return TransactionsSourceFactory::$instances[$source];
    }

}
