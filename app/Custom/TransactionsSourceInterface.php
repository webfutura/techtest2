<?php

namespace App\Custom;

/**
 * Allows retrieving transactions from a specific source.
 */
interface TransactionsSourceInterface
{

    /**
     * @return object[]
     */
    function retrieve();
}
