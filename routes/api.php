<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Custom\TransactionsSourceFactory;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/transactions', function(Request $request) {

    try {
        $source = TransactionsSourceFactory::getInstance($request->get('source'));
    }
    catch (\Throwable $e) {
        return response()->json(['error' => 'Supplied source is invalid.'], '400');
    }

    return $source->retrieve();
});
